package com.example.android.laydemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    public void fade(View view) {
        ImageView homer = (ImageView) findViewById(R.id.homer);
        ImageView bender = (ImageView) findViewById(R.id.bender);

//        bender.animate().alpha(0f).setDuration(2000);
//        bender.animate().translationYBy(1000f).setDuration(2000);
//        bender.animate().scaleX(0.5f).scaleY(0.5f).setDuration(2000);
//        bender.animate().rotation(1800f).setDuration(2000);

        homer.animate().alpha(1f).setDuration(2000);
        bender.animate()
                .translationXBy(1000f)
                .translationYBy(1000f)
                .rotationBy(3600)
                .setDuration(2000);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        ImageView bender = (ImageView) findViewById(R.id.bender);
        bender.setTranslationX(-1000f);
        bender.setTranslationY(-1000f);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
